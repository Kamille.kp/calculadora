package calculadora;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class FXMLDocumentController implements Initializable {
    
    @FXML
    private TextField field;
    
    @FXML
    private Label C;
    
    @FXML
    private Label K;
    
    @FXML
    private Label F;
    
    @FXML
    private Label label;
    
    @FXML
    private void calccelsius(ActionEvent event) {
        double num = Double.parseDouble(field.getText());
        double resk = num + 273;
        double resf = (num * 1.8) + 32;
        
        C.setText("Celsius: " + num);
        K.setText("Kelvin: " + resk);
        F.setText("Fahrenheit: " + resf);
    }
    
    @FXML
    private void calckelvin(ActionEvent event) {
        double num = Double.parseDouble(field.getText());
        double resc = num - 273;
        double resf = (num - 273.15) * 1.8 + 32;
        
        K.setText("Kelvin: " + num);
        C.setText("Celsius: " + resc);
        F.setText("Fahrenheit: " + resf);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }       
}
